FROM python:3-slim

RUN pip install --no-cache-dir ansible ansible-lint pylint
RUN apt-get update -y && \
    apt-get install openssh-client -y && \
    rm -rf /var/lib/apt/lists/*
